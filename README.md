# ScolBourse

Réalisé par :

 - Lucas GEHIN
 - Gregory BATON
 - Guillaume DEGESNE

 Sous la tutelle de Gérome CANALS
 
## Installation

### Base de données

Exécuter sur phpmyadmin le script `scol_bourse.sql`

Le script `data.sql` rentre dans la base les rôles d'utilisateurs et le compte super-administrateur admin/admin

### Virtual Host

Sur votre environnement apache, configurez le virtual host comme suit :

	<VirtualHost *:80>
        	ServerName      scolbourse
        	DocumentRoot    /www/scolbourse
        	<Directory "/www/scolbourse">
                	DirectoryIndex index.php
                	AllowOverride All
               	Order allow,deny
               	Allow from all
        	</Directory>
	</VirtualHost>
	
L'application est donc disponible à l'adresse scolbourse/

