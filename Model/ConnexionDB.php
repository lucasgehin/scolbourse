<?php

namespace Model;
use Illuminate\Database\Capsule\Manager as DB;

class ConnexionDB
{
   

    public static function createDB(Array $config)
    {
        $host = $config['host'];
        $base = $config['base'];
        $user = $config['user'];
        $pass = $config['pass'];
        $caps = new DB;
        $caps->addConnection( array(
         'driver' => 'mysql',
         'host' => $host,
         'database' => $base,
         'username' => $user,
         'password' => $pass,
         'charset' => 'utf8',
         'collation' => 'utf8_unicode_ci',
         'prefix' => '',
        ));
        // Make this Capsule instance available globally via static methods...
        $caps->setAsGlobal();
        // Setup the Eloquent ORM...
        $caps->bootEloquent();
    }
}