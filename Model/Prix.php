<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Prix extends Eloquent {
    protected $table = 'prix';
    public $timestamps=false;

    public function manuel()
    {
    	return $this->belongsTo('Model\Manuel', 'id_manuel');
    }
}