<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Manuel extends Eloquent {
    protected $table = 'manuel';
    protected $primaryKey = 'id';
    public $timestamps=false;

    public function etat()
    {
    	return $this->belongsTo('Model\Etat', 'id_etat');
    }
}