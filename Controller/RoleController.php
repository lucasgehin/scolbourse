<?php
namespace Controller;
class RoleController {
	public function listRole(){
		$app = \Slim\Slim::getInstance();
		$aff = array();

		$role_vide = true;

		$role = new \Model\Role();
		$ro=$role->all();
		if(!$ro->isEmpty()){
			$aff['status']=200;
			$role_vide=false;
			$i =0;
			foreach($ro as $r){
				$aff['role '.$i] = array( 'id' => $r->id,'libelle' => $r->libelle);
				$i++;
			}
		}

		if($role_vide){
			$aff['status']=500;
			$aff['error']='Aucun role dans la base.';
		}

		echo json_encode($aff);
	}
}