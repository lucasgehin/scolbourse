<?php
namespace Controller;
class IndexController {
    public function index()
    {
        $user = new \Model\User();
        $user_logged = $user->where("login", "=", $_SESSION['user'])->get();

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $twig->addGlobal('user', $user_logged[0]);
        $tmpl = $twig->loadTemplate('index.html.twig');
        $tmpl->display(array());
    }

    public function bourse($id)
    {
        $user = new \Model\User();
        $user_logged = $user->where("login", "=", $_SESSION['user'])->get();

        $fam = new \Model\Famille();
        $famille = $fam->where('id','=',$id)->get();
        if (!$famille->isEmpty()){

        } else {
            $app = \Slim\Slim::getInstance();
            $app->redirect('/');
        }

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $twig->addGlobal('user', $user_logged[0]);
        $tmpl = $twig->loadTemplate('bourse.html.twig');
        $tmpl->display(array('famille' => $famille[0]));
    }

    public function enfant($id_famille, $id_enfant)
    {
        $user = new \Model\User();
        $user_logged = $user->where("login", "=", $_SESSION['user'])->get();

        $enf = new \Model\Enfant();
        $enfant = $enf->where('id', '=', $id_enfant)->where('id_famille', '=', $id_famille)->get();
        if (!$enfant->isEmpty()){

        } else {
            $app = \Slim\Slim::getInstance();
            $app->redirect('/');
        }
        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $twig->addGlobal('user', $user_logged[0]);
        $tmpl = $twig->loadTemplate('enfant.html.twig');
        $tmpl->display(array('enfant' => $enfant[0]));
    }

    public function notFound()
    {
        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('404.html.twig');
        $tmpl->display(array());
    }
}