<?php
namespace Controller;
class TauxController {
    public function listeTaux()
    {
        $aff = array();
        $taux = new \Model\Taux();
        $tx = $taux->where('id', '=', 1)->get();
        if (!$tx->isEmpty()) {
            $aff['status'] = 200;
            $aff['taux_dossier'] = $tx[0]->frais_dossier;
            $aff['taux_envoi'] = $tx[0]->frais_envoi;
        } else {
            $aff['status']=500;
            $aff['error']='Aucun taux dans la base.';
        }
        echo json_encode($aff);
    }

    public function modifTaux()
    {
        $aff = array();

        if (isset($_POST['taux_dossier']) && isset($_POST['taux_envoi'])) {
            $taux = new \Model\Taux();
            $tx = $taux->where('id', '=', 1)->get();
            if (!$tx->isEmpty()) {
                $tx[0]->frais_dossier = $_POST['taux_dossier'];
                $tx[0]->frais_envoi = $_POST['taux_envoi'];
                $tx[0]->save();
            } else {
                $taux->id = 1;
                $taux->frais_dossier = $_POST['taux_dossier'];
                $taux->frais_envoi = $_POST['taux_envoi'];
                $taux->save();
            }
            $aff['status'] = 200;
            $aff['message'] = "Frais modifiés avec succès !";
        } else {
            $aff['status'] = 500;
            $aff['error'] = "Paramètres manquants.";
        }
        echo json_encode($aff);
    }
}