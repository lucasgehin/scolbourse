<?php
namespace Controller;
class PrixController {
	public function listPrix(){
		$app = \Slim\Slim::getInstance();
		$aff = array();

		$prix_vide = true;

		$prix = new \Model\Prix();
		$pr=$prix->all();
		if(!$pr->isEmpty()){
			$aff['status']=200;
			$prix_vide=false;
			$i =0;
			foreach($pr as $p){
				$aff['prix '.$i] = array( 'montant' => $p->montant,'id_manuel' => $p->id_manuel);
				$i++;
			}
		}

		if($prix_vide){
			$aff['status']=500;
			$aff['error']='Aucun prix dans la base.';
		}

		echo json_encode($aff);
	}
}