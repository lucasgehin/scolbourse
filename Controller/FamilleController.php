<?php
namespace Controller;
class FamilleController {
    public function listFamilles(){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $f_vide = true;

        $fam = new \Model\Famille();
        $familles=$fam->all();
        if(!$familles->isEmpty()){
            $aff['status']=200;
            $f_vide=false;
            $i = 0;
            foreach($familles as $f){
                $aff['famille '.$i] = array( 'id' => $f->id, 'nom' => $f->nom, 'prenom_resp' => $f->prenom_resp, 
                    'adresse' => $f->adresse , 'complement' => $f->complement , 'CP' => $f->cp , 'ville' => $f->ville , 'tel' => $f->tel , 'link' => "famille/".$f->id);
                $i++;
            }
        }

        if($f_vide){
            $aff['status']=500;
            $aff['error']='Aucune famille dans la base.';
        }

        echo json_encode($aff);
    }

    public function detailFamille($id){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $f_vide = true;

        $fam = new \Model\Famille();
        $famille=$fam->where('id','=',$id)->get();
        if(!$famille->isEmpty()){
            $aff['status']=200;
            $f_vide=false;
                $aff['famille'] = array( 'id' => $famille[0]->id, 'nom' => $famille[0]->nom, 'prenom' => $famille[0]->prenom_resp, 
                    'adresse' => $famille[0]->adresse , 'complement' => $famille[0]->complement , 'CP' => $famille[0]->cp , 'ville' => $famille[0]->ville , 'tel' => $famille[0]->tel,
                    'mail' => $famille[0]->mail, 'notes' => $famille[0]->notes,'adherent' => $famille[0]->adherent,'frais' => $famille[0]->frais,
                    'nom_fact' => $famille[0]->nom_fact, 'prenom_fact' => $famille[0]->prenom_fact, 
                    'adresse_fact' => $famille[0]->adresse_fact , 'complement_fact' => $famille[0]->complement_fact , 'CP_fact' => $famille[0]->cp_fact , 'ville_fact' => $famille[0]->ville_fact);
        }

        if($f_vide){
            $aff['status']=500;
            $aff['error']='Id incorrect.';
        }

        echo json_encode($aff);
    }

	public function nomFamille(){
		$aff = array();
		$f_vide = true;

		$fam = new \Model\Famille();
		$familles=$fam->select('nom')->distinct()->get();
		if(!$familles->isEmpty()){
			$f_vide=false;
			$i = 0;
			foreach($familles as $f){
				$aff[] = array('nom' => $f->nom);
				$i++;
			}
		}

		if($f_vide){
			$aff['status']=500;
			$aff['error']='Aucune famille dans la base.';
		}

		echo json_encode($aff);
	}

	public function rechercheFamille(){
		$aff = array();
		$ligne = array();
		$f_vide = true;
		$famille = new \Model\Famille();

		if($_POST['nom']!="" && $_POST['mail']!="" && $_POST['num_dossier']!="" && $_POST['num_exemplaire']!=""){
            $exemplaires = new \Model\Exemplaire();
            $exemplaire = $exemplaires->find($_POST['num_exemplaire']);
            if($_POST['num_dossier'] == $exemplaire->id_famille_depot){
	            $fam = $famille->where('id','=',$_POST['num_dossier'])->where('nom','like','%'.$_POST['nom'].'%')->where('mail','like','%'.$_POST['mail'].'%')->get();
            }else{
                $aff['status']=500;
                $aff['error']='Aucune famille ne correspond à cette recherche...'; 
            }
        }
        if($_POST['nom']=="" && $_POST['mail']=="" && $_POST['num_dossier']=="" && $_POST['num_exemplaire']!=""){
            $exemplaires = new \Model\Exemplaire();
            $exemplaire = $exemplaires->find($_POST['num_exemplaire']);
            $fam = $famille->where('id','=',$exemplaire->id_famille_depot)->get();
        }
        if($_POST['nom']=="" && $_POST['mail']=="" && $_POST['num_dossier']!="" && $_POST['num_exemplaire']==""){
            $fam = $famille->where('id','=',$_POST['num_dossier'])->get();
        }
        if($_POST['nom']=="" && $_POST['mail']!="" && $_POST['num_dossier']=="" && $_POST['num_exemplaire']==""){
            $fam = $famille->where('mail','like','%'.$_POST['mail'].'%')->get();
        }
        if($_POST['nom']!="" && $_POST['mail']=="" && $_POST['num_dossier']=="" && $_POST['num_exemplaire']==""){
            $fam = $famille->where('nom','like','%'.$_POST['nom'].'%')->get();
        }
		if($_POST['nom'] !='' && $_POST['mail'] != '' && $_POST['num_dossier']!="" && $_POST['num_exemplaire']==""){
			$fam = $famille->where('nom','like','%'.$_POST['nom'].'%')->where('mail','like %'.$_POST['mail'].'%')->where('id','=',$_POST['num_dossier'])->get();
			}
		if($_POST['nom'] !='' && $_POST['mail'] != '' && $_POST['num_dossier']=="" && $_POST['num_exemplaire']!=""){
			$exemplaires = new \Model\Exemplaire();
            $exemplaire = $exemplaires->find($_POST['num_exemplaire']);
            $fam = $famille->where('nom','like','%'.$_POST['nom'].'%')->where('mail','like %'.$_POST['mail'].'%')->where('id','=',$exemplaire->id_famille_depot)->get();
		}
        if($_POST['nom']!="" && $_POST['mail']=="" && $_POST['num_dossier']!="" && $_POST['num_exemplaire']!=""){
            $exemplaires = new \Model\Exemplaire();
            $exemplaire = $exemplaires->find($_POST['num_exemplaire']);
            if($_POST['num_dossier'] == $exemplaire->id_famille_depot){
                $fam = $famille->where('nom','like','%'.$_POST['nom'].'%')->where('id','=',$_POST['num_dossier'])->get();
            }
        }
        if($_POST['nom']=="" && $_POST['mail']!="" && $_POST['num_dossier']!="" && $_POST['num_exemplaire']!=""){
            $exemplaires = new \Model\Exemplaire();
            $exemplaire = $exemplaires->find($_POST['num_exemplaire']);
            if($_POST['num_dossier'] == $exemplaire->id_famille_depot){
                $fam = $famille->where('mail','like %'.$_POST['mail'].'%')->where('id','=',$_POST['num_dossier'])->get();
            }
        }
        if($_POST['nom']!="" && $_POST['mail']!="" && $_POST['num_dossier']=="" && $_POST['num_exemplaire']==""){
            $fam = $famille->where('nom','like','%'.$_POST['nom'].'%')->where('mail','like %'.$_POST['mail'].'%')->get();
        }
        if($_POST['nom']=="" && $_POST['mail']=="" && $_POST['num_dossier']!="" && $_POST['num_exemplaire']!=""){
            $exemplaires = new \Model\Exemplaire();
            $exemplaire = $exemplaires->find($_POST['num_exemplaire']);
            if($_POST['num_dossier'] == $exemplaire->id_famille_depot){
                $fam = $famille->where('id','=',$_POST['num_dossier'])->get();
            }
        }
        if($_POST['nom']=="" && $_POST['mail']!="" && $_POST['num_dossier']=="" && $_POST['num_exemplaire']!=""){
            $exemplaires = new \Model\Exemplaire();
            $exemplaire = $exemplaires->find($_POST['num_exemplaire']);
            $fam = $famille->where('mail','like %'.$_POST['mail'].'%')->where('id','=',$exemplaire->id_famille_depot)->get();
        }
        if($_POST['nom']=="" && $_POST['mail']!="" && $_POST['num_dossier']!="" && $_POST['num_exemplaire']==""){
            $fam = $famille->where('mail','like %'.$_POST['mail'].'%')->where('id','=',$_POST['num_dossier'])->get();
        }
        if($_POST['nom']!="" && $_POST['mail']=="" && $_POST['num_dossier']=="" && $_POST['num_exemplaire']!=""){
            $exemplaires = new \Model\Exemplaire();
            $exemplaire = $exemplaires->find($_POST['num_exemplaire']);
            $fam = $famille->where('nom','like','%'.$_POST['nom'].'%')->where('id','=',$exemplaire->id_famille_depot)->get();
        }
        if($_POST['nom']!="" && $_POST['mail']=="" && $_POST['num_dossier']!="" && $_POST['num_exemplaire']==""){
            $fam = $famille->where('nom','like','%'.$_POST['nom'].'%')->where('id','=',$_POST['num_dossier'])->get();
        }
        if(isset($fam)){
    		if(!$fam->isEmpty()){
    			$aff['status']=200;
    			$f_vide=false;
    			foreach($fam as $f){
    				$ligne[] = array( 'id' => $f->id, 'nom' => $f->nom, 'prenom' => $f->prenom_resp, 
    					'ville' => $f->ville , 'tel' => $f->tel , 'mail' => $f->mail, 'link' => "famille/".$f->id);
    			}
    			$aff['famille'] = $ligne;
    		}
        }

		if($f_vide){
			$aff['status']=500;
			$aff['error']='Aucune famille ne correspond à cette recherche...';
		}

		echo json_encode($aff);
	}

    public function verifAjoutFamille() {
        $aff = array() ;
        $contenu = true ;
        $famille = new \Model\Famille();

        if (isset ($_POST['nom']) && isset ($_POST['prenom']) && isset ($_POST['adresse']) &&
            isset ($_POST['cp']) && isset ($_POST['ville']) && isset ($_POST['tel']) &&
            isset ($_POST['mail'])) {

            if ($_POST['ajouter_fact'] == "false") {

                $famille->nom = htmlspecialchars($_POST['nom']);
                $famille->prenom_resp = htmlspecialchars($_POST['prenom']);
                $famille->adresse = htmlspecialchars($_POST['adresse']);
                $famille->complement = htmlspecialchars($_POST['complement']);
                $famille->cp = $_POST['cp'];
                $famille->ville = htmlspecialchars($_POST['ville']);
                $famille->tel = $_POST['tel'];
                $famille->mail = htmlspecialchars($_POST['mail']);
                $famille->notes = htmlspecialchars($_POST['notes']);
                $famille->adherent = ($_POST['adherent'] == "true" ? 1 : 0);
                $famille->frais = ($_POST['frais'] == "true" ? 1 : 0);

                $famille->adresse_fact = htmlspecialchars($_POST['adresse']);
                $famille->complement_fact = htmlspecialchars($_POST['complement']);
                $famille->cp_fact = $_POST['cp'];
                $famille->ville_fact = htmlspecialchars($_POST['ville']);
                $famille->nom_fact = htmlspecialchars($_POST['nom']);
                $famille->prenom_fact = htmlspecialchars($_POST['prenom']);
                $famille->save();

            } else {

                if (isset ($_POST['nom_fact']) && isset ($_POST['prenom_fact']) &&
                    isset ($_POST['adresse_fact']) && isset ($_POST['cp_fact']) &&
                    isset ($_POST['ville_fact'])) {

                    $famille->nom = htmlspecialchars($_POST['nom']);
                    $famille->prenom_resp = htmlspecialchars($_POST['prenom']);
                    $famille->adresse = htmlspecialchars($_POST['adresse']);
                    $famille->complement = htmlspecialchars($_POST['complement']);
                    $famille->cp = $_POST['cp'];
                    $famille->ville = htmlspecialchars($_POST['ville']);
                    $famille->tel = $_POST['tel'];
                    $famille->mail = htmlspecialchars($_POST['mail']);
                    $famille->notes = htmlspecialchars($_POST['notes']);
                    $famille->adherent = ($_POST['adherent'] == "true" ? 1 : 0);
                    $famille->frais = ($_POST['frais'] == "true" ? 1 : 0);

                    $famille->adresse_fact = htmlspecialchars($_POST['adresse_fact']);
                    $famille->complement_fact = htmlspecialchars($_POST['complement_fact']);
                    $famille->cp_fact = $_POST['cp_fact'];
                    $famille->ville_fact = htmlspecialchars($_POST['ville_fact']);
                    $famille->nom_fact = htmlspecialchars($_POST['nom_fact']);
                    $famille->prenom_fact = htmlspecialchars($_POST['prenom_fact']);
                    $famille->save();

                } else {
                    $contenu = false ;
                }
            }

        } else {
            $contenu = false ;
        }

        if ($contenu) {
            $aff['status'] = 200 ;
            $aff['message'] = 'Famille insérée avec succès.' ;
        } else {
            $aff['status'] = 500 ;
            $aff['error'] = 'Données manquantes.' ;
        }
        echo json_encode($aff);
    }

    public function modifierFamille() {
        $aff = array() ;
        $contenu = true ;
        $familles = new \Model\Famille();

        $app = \Slim\Slim::getInstance(); 
        $request = $app -> request() ;
        $var = $request -> put() ;

        if (isset ($var['nom']) && isset ($var['prenom']) && isset ($var['adresse']) &&
            isset ($var['cp']) && isset ($var['ville']) && isset ($var['tel']) &&
            isset ($var['mail'])) {

            $famille = $familles -> where('id', '=', $var['id']) -> get();
            if (! $famille -> isEmpty()) {

                

                    if (isset ($var['nom_fact']) && isset ($var['prenom_fact']) &&
                        isset ($var['adresse_fact']) && isset ($var['cp_fact']) &&
                        isset ($var['ville_fact'])) {

                        $famille[0]->nom = htmlspecialchars($var['nom']);
                        $famille[0]->prenom_resp = htmlspecialchars($var['prenom']);
                        $famille[0]->adresse = htmlspecialchars($var['adresse']);
                        $famille[0]->complement = htmlspecialchars($var['complement']);
                        $famille[0]->cp = $var['cp'];
                        $famille[0]->ville = htmlspecialchars($var['ville']);
                        $famille[0]->tel = $var['tel'];
                        $famille[0]->mail = htmlspecialchars($var['mail']);
                        $famille[0]->notes = htmlspecialchars($var['notes']);
                        $famille[0]->adherent = ($var['adherent'] == "true" ? 1 : 0);
                        $famille[0]->frais = ($var['frais'] == "true" ? 1 : 0);

                        $famille[0]->adresse_fact = htmlspecialchars($var['adresse_fact']);
                        $famille[0]->complement_fact = htmlspecialchars($var['complement_fact']);
                        $famille[0]->cp_fact = $var['cp_fact'];
                        $famille[0]->ville_fact = htmlspecialchars($var['ville_fact']);
                        $famille[0]->nom_fact = htmlspecialchars($var['nom_fact']);
                        $famille[0]->prenom_fact = htmlspecialchars($var['prenom_fact']);
                        $famille[0]->save();

                        $dossier_depot = new \Model\Depot();
                        $dossier = $dossier_depot->where('id_famille', '=', $famille[0]->id)->get();
                        $dossier_achat = new \Model\Achat();
                        $dossier_ach = $dossier_achat->where('id_famille', '=', $famille[0]->id)->get();
                        $taux_dossier = 0;
                        $taux = new \Model\Taux();
                        $tx = $taux->where('id', '=', 1)->get();
                        if (!$tx->isEmpty() && $famille[0]->frais == 0) {
                            $taux_dossier = $tx[0]->frais_dossier;
                        }
                        if (!$dossier->isEmpty()) {
                            $dossier[0]->frais = $dossier[0]->montant * ($taux_dossier/100);
                            $dossier[0]->save();
                        }

                        if (!$dossier_ach->isEmpty()) {
                            $dossier_ach[0]->frais = $dossier_ach[0]->montant * ($taux_dossier/100);
                            $dossier_ach[0]->save();
                        }

                    } else {
                        $contenu = false ;
                    }
                }

            

        } else {
            $contenu = false ;
        }

        if ($contenu) {
            $aff['status'] = 200 ;
            $aff['message'] = 'Modification effectué avec succès.' ;
        } else {
            $aff['status'] = 500 ;
            $aff['error'] = 'Erreur de modification.' ;
        }
        echo json_encode($aff);
    }

    public function exporterFamille() {
        $error = true;
        $message = "";

        $familles = new \Model\Famille() ;
        $allfamille = $familles->All();        

        if (! $allfamille -> isEmpty()) {
            $file = fopen('familles.csv', 'w+');
            
            foreach($allfamille as $famille) {
                $array = $famille->toArray();
                fputcsv($file, $array);
            }

            fclose($file);

            $error = false ;
            $message = "Familles sauvegardées avec succès dans le fichier familles.csv .";

        } else {
            $message = "Erreur : Pas de familles à exporter.";
        }  

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('validation_export.html.twig');
        $tmpl->display(array("error" => $error,
                             "message" => $message));    
    }

    public function importerFamille() {
        $error = true;
        $message = "";
        $row = 0;

        if (isset($_FILES['csv'])) {

            $mimes = array('application/vnd.ms-excel','text/csv');
            if(in_array($_FILES['csv']['type'],$mimes)){

                $file = fopen($_FILES['csv']['name'], "r") ;

                while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {

                    $num = count($data);
                    if ($num == 18) {

                        $familles = new \Model\Famille();
                        $nomfamille = $familles -> where('nom', '=', htmlspecialchars($data[1])) -> get();
                        $prenomfamille = $familles -> where('prenom_resp', '=', htmlspecialchars($data[2])) -> get();
                        $emailfamille = $familles -> where('mail', '=', htmlspecialchars($data[8])) -> get();

                        if ($nomfamille -> isEmpty() &&
                            $prenomfamille -> isEmpty() &&
                            $emailfamille -> isEmpty()) {

                            $newfamille = new \Model\Famille();           
                            $newfamille->nom = htmlspecialchars($data[1]);
                            $newfamille->prenom_resp = htmlspecialchars($data[2]);
                            $newfamille->adresse = htmlspecialchars($data[3]);
                            $newfamille->complement = htmlspecialchars($data[4]);
                            $newfamille->cp = $data[5];
                            $newfamille->ville = htmlspecialchars($data[6]);
                            $newfamille->tel = $data[7];
                            $newfamille->mail = htmlspecialchars($data[8]);
                            $newfamille->notes = htmlspecialchars($data[9]);
                            $newfamille->adherent = $data[10];
                            $newfamille->frais = $data[11];
                            $newfamille->adresse_fact = htmlspecialchars($data[12]);
                            $newfamille->complement_fact = htmlspecialchars($data[13]);
                            $newfamille->cp_fact = $data[14];
                            $newfamille->ville_fact = htmlspecialchars($data[15]);
                            $newfamille->nom_fact = htmlspecialchars($data[16]);
                            $newfamille->prenom_fact = htmlspecialchars($data[17]);
                            $newfamille->save();

                            $error = false;
                            $row ++;
                        }

                    }

                }

                if (! $error) {
                    $message = $row." famille(s) importée(s) avec succès.";
                } else {
                    $message = "Aucune familles à importer.";
                }

                fclose($file);

            } else {
                $message = "Erreur : Fichier au mauvais format.";
            }

        } else {
            $message = "Erreur : Pas de fichier.";
        }

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('validation_import.html.twig');
        $tmpl->display(array("error" => $error,
                             "message" => $message));
    }
}