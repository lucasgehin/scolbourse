<?php
namespace Controller;
class UserController {
    public function login()
    {
        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('login.html.twig');
        $tmpl->display(array());
    }

    public function verifLogin()
    {
        $error = true;
        $login = $_POST['login'];
        $password = md5($_POST['password']);

        $user = new \Model\User();
        $result_user = $user->where('login', '=', $login)->get();
        if (isset($result_user[0])) {
            if ($result_user[0]->pass == $password) {
                $error = false;
                $_SESSION['logged'] = true;
                $_SESSION['user'] = $result_user[0]->login;
                $app = \Slim\Slim::getInstance() ;
                $app -> redirect("/") ;
            }
        }
        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('login.html.twig');
        $tmpl->display(array("error" => $error, "login" => $_POST['login']));
    }

    public function index(){
        $user = new \Model\User();
        $user_logged = $user->where("login", "=", $_SESSION['user'])->get();
        $role = new \Model\Role();
        $ro = $role->find($user_logged[0]->id_role);

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $twig->addGlobal('user', $user_logged[0]);
        $tmpl = $twig->loadTemplate('parametre.html.twig');
        $tmpl->display(array("user" => $user_logged[0], "role" => $ro));
    }

    public function ajoutCompte()
    {
        $res = array();
        if (isset($_POST['login']) && isset($_POST['password'])) {
            $login = htmlspecialchars($_POST['login']);
            $password = htmlspecialchars($_POST['password']);
            
            $user = new \Model\User();
            $us = $user->where('login', '=', $login)->get();
            if ($us->isEmpty()) {
                $user->login = $login;
                $user->pass = md5($password);
                $user->id_role = 1;
                $user->save();
                    
                $res['login'] = $login;
                $res['password'] = $password;
                $res['status'] = 200;
            } else {
                $res['status'] = 500;
                $res['error'] = "Cet utilisateur est déjà pris.";
            }
            
        } else {
            $res['status'] = 500;
            $res['error'] = "Paramètres manquants.";
        }
        echo json_encode($res);
    }

    public function listUser()
    {
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $user_vide = true;

        $user = new \Model\User();
        $us = $user->all();
        if(!$us->isEmpty()){
            $user_vide=false;
            $i =0;
            foreach($us as $u){
                $aff[] = array( 'id' => $u->id,'text' => $u->login);
                $i++;
            }
        }

        if($user_vide){
            $aff['status']=500;
            $aff['error']='Aucun user dans la base.';
        }

        echo json_encode($aff);
    }

    public function listBenevole()
    {
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $user_vide = true;

        $user = new \Model\User();
        $us = $user->where('id_role','=',1)->get();
        if(!$us->isEmpty()){
            $user_vide=false;
            $i =0;
            foreach($us as $u){
                $aff[] = array( 'id' => $u->id,'text' => $u->login);
                $i++;
            }
        }

        if($user_vide){
            $aff['status']=500;
            $aff['error']='Aucun benevole dans la base.';
        }

        echo json_encode($aff);
    }

    public function isBenevole($id){
        $benev = false;
        $user = new \Model\User();
        $us = $user->where('id','=',$id)->get();
        if(!$us->isEmpty()){
            if($us[0]->id_role == 1){
                $benev = true;
            }
        }
        echo json_encode($benev);

    }

    public function detailUser($id){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $user_vide = true;

        $user = new \Model\User();
        $us=$user->where('id','=',$id)->get();
        if(!$us->isEmpty()){
            $aff['status']=200;
            $user_vide=false;
            $role = new \Model\Role();
            $ro = $role->find($us[0]->id_role)->get();
            $aff['user'] = array( 'id' => $us[0]->id,'login' => $us[0]->login ,'mot de passe' => $us[0]->pass ,'role' => array('id' => $ro[0]->id, 'libelle' => $ro[0]->libelle , 'link' => 'roles/'.$ro[0]->id));      
        }

        if($user_vide){
            $aff['status']=500;
            $aff['error']='Id incorrect.';
        }

        echo json_encode($aff);
    }

    public function modifRole(){

        $aff = array();
        $contenu = true;
        $users = new \Model\User();

        $app = \Slim\Slim::getInstance(); 
        $request = $app -> request() ;
        $var = $request -> put() ;

        if(isset ($var['id_role'])){
            $user = $users->where('id','=',$var['id_user'])->get();
            if(!$user->isEmpty()){
                $user[0] -> id_role = $var['id_role'];
                $user[0] -> save();
            }else{
                $contenu = false;
            }
        }else{
            $contenu = false;
        }
        if (! $contenu) {
            $aff['status'] = 500 ;
            $aff['error'] = 'Erreur de modification.';
        } else {
            $aff['status'] = 200 ;
            $aff['message'] = "Modification effectuée avec succès.";
        }
        echo json_encode($aff);
    }

    public function modifPassword(){

        $aff = array();
        $contenu = true;
        $users = new \Model\User();

        $app = \Slim\Slim::getInstance(); 
        $request = $app -> request() ;
        $var = $request -> put() ;

        if(isset ($var['nv_pass']) && isset($var['confirm_nv_pass']) && isset($var['ancien_mdp_user'])){
            $user = $users->where('login','=',$_SESSION['user'])->get();
            if(!$user->isEmpty()){
                if($var['confirm_nv_pass'] == $var['nv_pass']){
                    if(md5($var['ancien_mdp_user']) == $user[0]->pass){
                        $user[0]->pass = md5($var['nv_pass']);
                        $user[0]->save();
                    }else{
                        $contenu = false;
                        $aff['status'] = 500 ;
                        $aff['error'] = 'Mot de passe incorrect.';
                    }
                }else{
                    $contenu = false;
                    $aff['status'] = 500 ;
                    $aff['error'] = 'Les mots de passe ne sont pas identiques.';
                }
            }else{
                $contenu = false;
            }
        }else{
            $contenu = false;
        }
        if ($contenu) {
            $aff['status'] = 200 ;
            $aff['message'] = "Modification effectuée avec succès.";
        }
        echo json_encode($aff);
    }

    public function logout()
    {
        if (isset($_SESSION['logged'])) {
            session_destroy();

        }
        $app = \Slim\Slim::getInstance() ;
        $app -> redirect("/") ;
    }
}