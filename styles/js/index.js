$(document).ready(function () {
    $('.menu .index').addClass("active");
    $('.ui.checkbox').checkbox();

    $('#button_rechercher_famille').on('click', function (e) {
        e.preventDefault();
        if (($('#rechercher_nom').val() == "") && ($('#rechercher_mail').val() == "") && ($('#rechercher_dossier').val() == "") && ($('#rechercher_exemplaire').val() == "")) {
            $('#modal_erreur .content').html('Veuillez compléter au moins un champ du formulaire de recherche.');
            $('#modal_erreur').modal('show');
        } else {
            $.ajax({
                type: "POST",
                url: "/api/recherche/familles",
                data: {nom: $('#rechercher_nom').val(), mail: $('#rechercher_mail').val(), num_dossier: $('#rechercher_dossier').val(), num_exemplaire: $('#rechercher_exemplaire').val()}
            }).done(function (msg) {
                if (msg.status == 200) {
                    var content = "<table class='ui striped table'><thead><tr><th>Nom</th><th>Prénom</th><th>Ville</th><th>E-mail</th><th>Téléphone</th></tr></thead><tbody>";
                    msg.famille.forEach(function (f) {
                        content += "<tr data-id='" + f.id + "' class='select_famille'><td>" + f.nom + "</td><td>" + f.prenom + "</td><td>" + f.ville + "</td><td>" + f.mail + "</td><td>" + f.tel + "</td></tr>";
                    });
                    content += "</tbody></table>";
                    $('#recherche .content').html(content);
                } else {
                    $('#recherche .content').html("<div class='not_found'>" + msg.error + "</div>");
                }
                $('#recherche').css('display', '');
                $('.select_famille').on('click', function (e) {
                    if($(this).data('id') !== undefined){
                        document.location = "/famille/" + $(this).data('id');
                    }
                });
            });
           
        }
    });

    $('#button_ajouter_famille').on('click', function (e) {
        e.preventDefault();
        $('#segment_ajouter_famille').css('display', '');
        $('#button_ajouter_famille').css('display', 'none');
        $('#ajouter_nom').focus();
    });
    $('#close_ajouter_famille, #annuler_ajouter_famille').on('click', function (e) {
        e.preventDefault();
        $('#segment_ajouter_famille').css('display', 'none');
        $('#button_ajouter_famille').css('display', '');
        $('#form_ajouter_famille')[0].reset();
        $('#facturation').css('display', 'none');
        $('.field').removeClass('error');
    });
    $('#ajouter_fact').change(function (e) {
        if ($(this).is(':checked')) {
            $('#facturation').css('display', 'none');
        } else {
            $('#facturation').css('display', '');
            $('#ajouter_nom_fact').focus();
        }
    });
    $('#form_ajouter_famille').submit(function (e) {
        e.preventDefault();
        $('.field').removeClass('error');
        $(this).addClass('loading');
        var ok_ajout = true;
        var message_erreur = "<ul>";
        var nom = $('#ajouter_nom').val();
        var prenom = $('#ajouter_prenom').val();
        var telephone = $('#ajouter_tel').val();
        var mail = $('#ajouter_mail').val();
        var adresse = $('#ajouter_adresse').val();
        var complement = $('#ajouter_complement').val();
        var cp = $('#ajouter_cp').val();
        var ville = $('#ajouter_ville').val();
        var adherent = $('#ajouter_adherent').prop('checked') ? true : false;
        var frais = $('#ajouter_frais').prop('checked') ? true : false;
        var fact = $('#ajouter_fact').prop('checked') ? false : true;
        var nom_fact = $('#ajouter_nom_fact').val();
        var prenom_fact = $('#ajouter_prenom_fact').val();
        var adresse_fact = $('#ajouter_adresse_fact').val();
        var complement_fact = $('#ajouter_complement_fact').val();
        var cp_fact = $('#ajouter_cp_fact').val();
        var ville_fact = $('#ajouter_ville_fact').val();
        var notes = $('#ajouter_notes').val();

        if (nom == "") {
            ok_ajout = false;
            $('#ajouter_nom').parent().addClass('error');
            message_erreur += "<li>Nom</li>";
        }
        if (prenom == "") {
            ok_ajout = false;
            $('#ajouter_prenom').parent().addClass('error');
            message_erreur += "<li>Prénom</li>";
        }
        if (telephone == "") {
            ok_ajout = false;
            $('#ajouter_tel').parent().addClass('error');
            message_erreur += "<li>Téléphone</li>";
        }
        if (mail == "") {
            ok_ajout = false;
            $('#ajouter_mail').parent().addClass('error');
            message_erreur += "<li>Adresse e-mail</li>";
        }
        if (adresse == "") {
            ok_ajout = false;
            $('#ajouter_adresse').parent().addClass('error');
            message_erreur += "<li>Adresse</li>";
        }
        if (cp == "") {
            ok_ajout = false;
            $('#ajouter_cp').parent().addClass('error');
            message_erreur += "<li>Code postal</li>";
        }
        if (ville == "") {
            ok_ajout = false;
            $('#ajouter_ville').parent().addClass('error');
            message_erreur += "<li>Ville</li>";
        }
        if (ok_ajout) {
            $.ajax({
                type: "POST",
                url: "/api/familles",
                data: {nom: nom, prenom: prenom, tel: telephone, mail: mail, adresse: adresse, complement: complement, cp: cp, ville: ville, adherent: adherent, frais: frais, ajouter_fact: fact, nom_fact: nom_fact, prenom_fact: prenom_fact, adresse_fact: adresse_fact, complement_fact: complement_fact, cp_fact: cp_fact, ville_fact: ville_fact, notes: notes }
            }).done(function (msg) {
                $('#segment_ajouter_famille').css('display', 'none');
                $('#button_ajouter_famille').css('display', '');
                $('#form_ajouter_famille')[0].reset();
                $('#facturation').css('display', 'none');
                $('.field').removeClass('error');
                $('#modal_succes .content').html(msg.message);
                $('#modal_succes').modal('show');
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');
    });


    function initDepotAchat (id_famille) {
        $('#recherche').css('display', 'none');
        $('#chercher_famille').css('display', 'none');
        $('#bourse').css('display', '');
    }
});