$('#modif_user_form').submit(function (e){
        e.preventDefault();
        $(this).addClass('loading');
        var login = $('#modif_user_login').val();
        var ancien_mdp = $('#ancien_mdp_user').val();
        var nv_pass = $('#nv_pass').val();
        var confirm_pass = $('#confirm_nv_pass').val();
        $.ajax({
            type: "PUT",
            url: "/api/user/pass/",
            data: {login: login , ancien_mdp_user: ancien_mdp , nv_pass: nv_pass , confirm_nv_pass: confirm_pass}
        }).done(function (msg) {
            if (msg.status == 200) {
                $('#modif_user_form').html("<div class='ui success message' style='display:block'><div class='header'>Mot de passe modifié pour</div><p> " + login + "<br />");
                $('#modif_user_form').removeClass('loading');
            } else {
                $('#modal_erreur .content').html(msg.error);
                $('#modal_erreur').modal('show');
            }
        });
        $(this).removeClass('loading');
    });