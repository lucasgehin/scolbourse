$(document).ready(function () {
    $('.menu .index').addClass("active");

    $('#modal_succes').modal({
        onHide : function () {
            location.reload(true);
        }
    });
    
    $('#form_modifier_enfant').submit(function (e) {
        var id_fam = $('#modifier_enfant').data('fam');
        var id = $('#modifier_enfant').data('id');
        e.preventDefault();
        $('.field').removeClass('error');
        $(this).addClass('loading');
        var ok_ajout = true;
        var message_erreur = "<ul>";
        var nom = $('#modifier_nom').val();
        var prenom = $('#modifier_prenom').val();
        var classe = $('#modifier_classe').val();
        if (nom == "") {
            ok_ajout = false;
            $('#modifier_nom').parent().addClass('error');
            message_erreur += "<li>Nom</li>";
        }
        if (prenom == "") {
            ok_ajout = false;
            $('#modifier_prenom').parent().addClass('error');
            message_erreur += "<li>Prénom</li>";
        }
        if (classe == "") {
            ok_ajout = false;
            $('#modifier_classe').parent().addClass('error');
            message_erreur += "<li>Classe</li>";
        }
        if (ok_ajout) {
            $.ajax({
                type: "PUT",
                url: "/api/enfants",
                data: {id: id, id_famille: id_fam, nom: nom, prenom: prenom, classe: classe}
            }).done(function (msg) {
                $('#modal_succes .content').html(msg.message);
                $('#modal_succes').modal('show');
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');
    });

    $('#button_supprimer_enfant').on('click', function (e) {
        var id_fam = $('#modifier_enfant').data('fam');
        var id = $('#modifier_enfant').data('id');
        e.preventDefault();
        $('#modal_confirm_supprimer').modal({
                closable  : false,
                onDeny    : function(){
                },
                onApprove : function() {
                    $.ajax({
                        type: "DELETE",
                        url: "/api/familles/" + id_fam + "/enfants/" + id
                    }).done(function (msg) {
                        window.location.assign("/famille/" + id_fam);
                    });
                }
              })
              .modal('setting', 'closable', false)
              .modal('show');
    });

});