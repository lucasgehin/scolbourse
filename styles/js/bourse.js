$(document).ready(function () {
    $('.menu .index').addClass("active");
    $('.ui.checkbox').checkbox();
    $('.menu .item').tab();
    init();
    initDepot();
    initAchat();

    $('#modal_succes').modal({
        onHide : function () {
            location.reload(true);
        }
    });

    $('#button_modifier_famille').on('click', function (e) {
        e.preventDefault();
        $('#famille_infos').css('display', 'none');
        $('#famille_modifier').css('display', '');
        $('#form_modifier_famille').addClass('loading');
        var id = $('#info_famille').data('id');
        $.ajax({
            type: "GET",
            url: "/api/familles/" + id
        }).done(function (msg) {
            $('#modifier_famille_nom').val(msg.famille.nom);
            $('#modifier_famille_prenom').val(msg.famille.prenom);
            $('#modifier_famille_tel').val(msg.famille.tel);
            $('#modifier_famille_mail').val(msg.famille.mail);
            $('#modifier_famille_adresse').val(msg.famille.adresse);
            $('#modifier_famille_complement').val(msg.famille.complement);
            $('#modifier_famille_cp').val(msg.famille.CP);
            $('#modifier_famille_ville').val(msg.famille.ville);
            $('#modifier_famille_nom_fact').val(msg.famille.nom_fact);
            $('#modifier_famille_prenom_fact').val(msg.famille.prenom_fact);
            $('#modifier_famille_adresse_fact').val(msg.famille.adresse_fact);
            $('#modifier_famille_complement_fact').val(msg.famille.complement_fact);
            $('#modifier_famille_cp_fact').val(msg.famille.CP_fact);
            $('#modifier_famille_ville_fact').val(msg.famille.ville_fact);
            $('#modifier_famille_notes').val(msg.famille.notes);
            if (msg.famille.adherent == 1) { $('#modifier_famille_adherent').attr('checked', ''); }
            if (msg.famille.frais == 1) { $('#modifier_famille_frais').attr('checked', ''); }
            $('#form_modifier_famille').removeClass('loading');
        });
    });
    $('#annuler_modifier_famille').on('click', function (e) {
        e.preventDefault();
        $('#famille_infos').css('display', '');
        $('#famille_modifier').css('display', 'none');
    });
    $('#form_modifier_famille').submit(function (e) {
        e.preventDefault();
        var id = $('#info_famille').data('id');
        $('.field').removeClass('error');
        $(this).addClass('loading');
        var ok_ajout = true;
        var message_erreur = "<ul>";
        var nom = $('#modifier_famille_nom').val();
        var prenom = $('#modifier_famille_prenom').val();
        var telephone = $('#modifier_famille_tel').val();
        var mail = $('#modifier_famille_mail').val();
        var adresse = $('#modifier_famille_adresse').val();
        var complement = $('#modifier_famille_complement').val();
        var cp = $('#modifier_famille_cp').val();
        var ville = $('#modifier_famille_ville').val();
        var adherent = $('#modifier_famille_adherent').prop('checked') ? true : false;
        var frais = $('#modifier_famille_frais').prop('checked') ? true : false;
        var nom_fact = $('#modifier_famille_nom_fact').val();
        var prenom_fact = $('#modifier_famille_prenom_fact').val();
        var adresse_fact = $('#modifier_famille_adresse_fact').val();
        var complement_fact = $('#modifier_famille_complement_fact').val();
        var cp_fact = $('#modifier_famille_cp_fact').val();
        var ville_fact = $('#modifier_famille_ville_fact').val();
        var notes = $('#modifier_famille_notes').val();
        if (nom == "") {
            ok_ajout = false;
            $('#modifier_famille_nom').parent().addClass('error');
            message_erreur += "<li>Nom</li>";
        }
        if (prenom == "") {
            ok_ajout = false;
            $('#modifier_famille_prenom').parent().addClass('error');
            message_erreur += "<li>Prénom</li>";
        }
        if (telephone == "") {
            ok_ajout = false;
            $('#modifier_famille_tel').parent().addClass('error');
            message_erreur += "<li>Téléphone</li>";
        }
        if (mail == "") {
            ok_ajout = false;
            $('#modifier_famille_mail').parent().addClass('error');
            message_erreur += "<li>Adresse e-mail</li>";
        }
        if (adresse == "") {
            ok_ajout = false;
            $('#modifier_famille_adresse').parent().addClass('error');
            message_erreur += "<li>Adresse</li>";
        }
        if (cp == "") {
            ok_ajout = false;
            $('#modifier_famille_cp').parent().addClass('error');
            message_erreur += "<li>Code postal</li>";
        }
        if (ville == "") {
            ok_ajout = false;
            $('#modifier_famille_ville').parent().addClass('error');
            message_erreur += "<li>Ville</li>";
        }
        if (nom_fact == "") {
            ok_ajout = false;
            $('#modifier_famille_nom_fact').parent().addClass('error');
            message_erreur += "<li>Nom pour facturation</li>";
        }
        if (prenom_fact == "") {
            ok_ajout = false;
            $('#modifier_famille_prenom_fact').parent().addClass('error');
            message_erreur += "<li>Prénom pour facturation</li>";
        }
        if (adresse_fact == "") {
            ok_ajout = false;
            $('#modifier_famille_adresse_fact').parent().addClass('error');
            message_erreur += "<li>Adresse pour facturation</li>";
        }
        if (cp_fact == "") {
            ok_ajout = false;
            $('#modifier_famille_cp_fact').parent().addClass('error');
            message_erreur += "<li>Code postal pour facturation</li>";
        }
        if (ville_fact == "") {
            ok_ajout = false;
            $('#modifier_famille_ville_fact').parent().addClass('error');
            message_erreur += "<li>Ville pour facturation</li>";
        }
        if (ok_ajout) {
            $.ajax({
                type: "PUT",
                url: "/api/familles",
                data: {id: id, nom: nom, prenom: prenom, adresse: adresse, complement: complement, cp: cp, ville: ville, tel: telephone, mail: mail, notes: notes, adherent: adherent, frais: frais, adresse_fact: adresse_fact, complement_fact: complement_fact, cp_fact: cp_fact, ville_fact: ville_fact, nom_fact: nom_fact, prenom_fact: prenom_fact}
            }).done(function (msg) {
                if (msg.status == 200) {
                    $('#modal_succes .content').html(msg.message);
                    $('#modal_succes').modal('show');
                } else {
                    $('#modal_erreur .content').html(msg.error);
                    $('#modal_erreur').modal('show');
                }
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');

    });

    $('#button_ajouter_enfant').on('click', function (e) {
        e.preventDefault();
        $(this).css('display', 'none');
        $('#ajout_enfant').css('display', '');
        $('#ajouter_nom').focus();
    });
    $('#annuler_ajouter_enfant').on('click', function (e) {
        e.preventDefault();
        $('#ajout_enfant').css('display', 'none');
        $('#button_ajouter_enfant').css('display', '');
        $('#form_ajouter_enfant')[0].reset();
        $('.field').removeClass('error');
    });
    $('#form_ajouter_enfant').submit(function (e) {
        e.preventDefault();
        var id = $('#info_famille').data('id');
        $('.field').removeClass('error');
        $(this).addClass('loading');
        var ok_ajout = true;
        var message_erreur = "<ul>";
        var nom = $('#ajouter_nom').val();
        var prenom = $('#ajouter_prenom').val();
        var classe = $('#ajouter_classe').val();
        if (nom == "") {
            ok_ajout = false;
            $('#ajouter_nom').parent().addClass('error');
            message_erreur += "<li>Nom</li>";
        }
        if (prenom == "") {
            ok_ajout = false;
            $('#ajouter_prenom').parent().addClass('error');
            message_erreur += "<li>Prénom</li>";
        }
        if (classe == "") {
            ok_ajout = false;
            $('#ajouter_classe').parent().addClass('error');
            message_erreur += "<li>Classe</li>";
        }
        if (ok_ajout) {
            $.ajax({
                type: "POST",
                url: "/api/enfants",
                data: {nom: nom, prenom: prenom, classe: classe, famille: id }
            }).done(function (msg) {
                $('#ajout_enfant').css('display', 'none');
                $('#button_ajouter_enfant').css('display', '');
                $('#form_ajouter_enfant')[0].reset();
                $('.field').removeClass('error');
                $('#modal_succes .content').html(msg.message);
                $('#modal_succes').modal('show');
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');
    });
    $('#depot_code_etat').keydown(function(e){ 
        if (e.which == 9) {
            $('#form_nouveau_depot').submit();
        }
    });
    $('#achat_code_exemplaire').keydown(function(e){ 
        if (e.which == 9) {
            $('#form_nouvel_achat').submit();
        }
    });

    $('#form_nouveau_depot').submit(function (e) {
        $(this).addClass('loading');
        e.preventDefault();
        var id_famille = $('#info_famille').data('id');
        var ok_ajout = true;
        $('.field').removeClass('error');
        var message_erreur = "<ul>";
        var id_manuel = $('#depot_code_manuel').val();
        var id_exemplaire = $('#depot_code_exemplaire').val();
        var id_etat = $('#depot_code_etat').val();
        if (id_manuel == "") {
            ok_ajout = false;
            message_erreur += "<li>Code manuel</li>";
            $('#depot_code_manuel').parent().addClass('error');
        }
        if (id_exemplaire == "") {
            ok_ajout = false;
            message_erreur += "<li>Code exemplaire</li>";
            $('#depot_code_exemplaire').parent().addClass('error');
        }
        if (id_etat == "") {
            ok_ajout = false;
            message_erreur += "<li>Code état</li>";
            $('#depot_code_etat').parent().addClass('error');
        }
        if (ok_ajout) {
            var id_enfant = "";
            if ($('#enfants_depot').css('display') != "none") {
                id_enfant = $('#enfants_depot_val').val();
            }
            $.ajax({
                type: "POST",
                url: "/api/exemplaires",
                data : {isbn_manuel: id_manuel, id_famille: id_famille, id_etat: id_etat, id_exemplaire: id_exemplaire, id_enfant: id_enfant}
            }).done(function (msg) {
                if (msg.status == 200) {
                    $('.notification').html(msg.message);
                    $('.notification').slideDown('fast');
                    window.setTimeout(closeNotif,5000);
                    initDepot();
                    $('#form_nouveau_depot')[0].reset();
                    $('#depot_code_manuel').focus();

                } else {
                    $('#modal_erreur .content').html(msg.error);
                    $('#modal_erreur').modal('show');
                }
                
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');
    });

    $('#form_depot_paiement').submit(function (e) {
        $(this).addClass('loading');
        e.preventDefault();
        var id_famille = $('#info_famille').data('id');
        var ok_ajout = true;
        $('.field').removeClass('error');
        var message_erreur = "<ul>";
        var montant = $('#depot_paiement_montant').val();
        if (montant == "") {
            ok_ajout = false;
            message_erreur += "<li>Montant</li>";
            $('#depot_paiement_montant').parent().addClass('error');
        }
        if (ok_ajout) {
            $.ajax({
                type: "POST",
                url: "/api/depot/paiement",
                data : {id_famille: id_famille, montant: montant}
            }).done(function (msg) {
                if (msg.status == 200) {
                    $('.notification').html(msg.message);
                    $('.notification').slideDown('fast');
                    window.setTimeout(closeNotif,5000);
                    initDepot();
                    $('#form_depot_paiement')[0].reset();
                } else {
                    $('#modal_erreur .content').html(msg.error);
                    $('#modal_erreur').modal('show');
                }
                
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');
    });

    $('#form_achat_paiement').submit(function (e) {
        $(this).addClass('loading');
        e.preventDefault();
        var id_famille = $('#info_famille').data('id');
        var ok_ajout = true;
        $('.field').removeClass('error');
        var message_erreur = "<ul>";
        var montant = $('#achat_paiement_montant').val();
        var mode = $('#achat_paiement_mode').val();
        if (montant == "") {
            ok_ajout = false;
            message_erreur += "<li>Montant</li>";
            $('#achat_paiement_montant').parent().addClass('error');
        }
        if (ok_ajout) {
            $.ajax({
                type: "POST",
                url: "/api/achat/paiement",
                data: {id_famille: id_famille, montant: montant, mode: mode}
            }).done(function (msg) {
                if (msg.status == 200) {
                    $('.notification').html(msg.message);
                    $('.notification').slideDown('fast');
                    window.setTimeout(closeNotif,5000);
                    initAchat();
                    $('#form_achat_paiement')[0].reset();
                } else {
                    $('#modal_erreur .content').html(msg.error);
                    $('#modal_erreur').modal('show');
                }
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');
    });

    $('#form_nouvel_achat').submit(function (e) {
        $(this).addClass('loading');
        e.preventDefault();
        var id_famille = $('#info_famille').data('id');
        var ok_ajout = true;
        $('.field').removeClass('error');
        var message_erreur = "<ul>";
        var id_exemplaire = $('#achat_code_exemplaire').val();
        if (id_exemplaire == "") {
            ok_ajout = false;
            message_erreur += "<li>Code exemplaire</li>";
            $('#achat_code_exemplaire').parent().addClass('error');
        }
        if (ok_ajout) {
            var id_enfant = "";
            if ($('#enfants_achat').css('display') != "none") {
                id_enfant = $('#enfants_achat_val').val();
            }
            $.ajax({
                type: "POST",
                url: "/api/achats",
                data : {id_famille: id_famille, id_exemp: id_exemplaire, id_enfant: id_enfant}
            }).done(function (msg) {
                if (msg.status == 200) {
                    $('.notification').html(msg.message);
                    $('.notification').slideDown('fast');
                    window.setTimeout(closeNotif,5000);
                    initAchat();
                    $('#form_nouvel_achat')[0].reset();
                    $('#achat_code_exemplaire').focus();

                } else {
                    $('#modal_erreur .content').html(msg.error);
                    $('#modal_erreur').modal('show');
                }
                
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');
    });

    function init () {
        var id = $('#info_famille').data('id');

        //infos de la famille
        $.ajax({
            type: "GET",
            url: "/api/familles/" + id
        }).done(function (msg) {
            if (msg.status == 200) {
                $('#famille_nom').text(msg.famille.nom);
                $('#famille_prenom').text(msg.famille.prenom);
                $('#famille_ville').text(msg.famille.ville);
                $('#famille_mail').text(msg.famille.mail);
                $('#famille_tel').text(msg.famille.tel);
            }
        });

        //enfants
        $.ajax({
            type: "GET",
            url: "/api/familles/" + id + "/enfants"
        }).done(function (msg) {
            if (msg.status == 200) {
                var content_select = "";
                var content = "<table class='ui striped table'><thead><tr><th>Nom</th><th>Prénom</th><th>Classe</th><th class='two wide'>Action</th></tr></thead><tbody>";
                msg.enfants.forEach(function (f) {
                    content_select += "<option value='" + f.id + "'>" + f.prenom + "</option>";
                    content += "<tr><td>" + f.nom + "</td><td>" + f.prenom + "</td><td>" + f.classe + "</td><td><a href='/famille/" + id + "/enfant/" + f.id + "'><i class='write icon'></i>Modifier</a></td><tr>";
                });
                $('#enfants_depot').css('display', '');
                $('#enfants_achat').css('display', '');
                $('#enfants_depot > select').html(content_select);
                $('#enfants_achat > select').html(content_select);
                $('#enfants_depot > select, #enfants_achat > select').dropdown();
                $('#content_enfants').prepend(content + "</tbody></table>");
            } else {
                $('#content_enfants').prepend("<div class='not_found' style='text-align:left'>Aucun enfant renseigné pour cette famille.</div>");
            }
        });

    }

    function initDepot() {
        var id = $('#info_famille').data('id');
        //depots
        $.ajax({
            type: "GET",
            url: "/api/depots/familles/" + id
        }).done(function (msg) {
            if (msg.status == 200) {
                var total = 0;
                var content = "<table class='ui celled striped table'><thead><tr><th>Code manuel</th><th>Code exemplaire</th><th>Titre</th><th class='center aligned'>Etat</th><th class='right aligned'>Tarif</th><th></th></tr></thead><tbody>";
                msg.depot.forEach(function (d) {
                    content += "<tr><td>" + d.manuel.isbn + "</td><td>" + d.id + "</td><td>" + d.manuel.titre + "</td><td class='center aligned'>" + d.etat.libelle + "</td><td class='right aligned'>" + d.prix + " €</td><td class='center aligned'>" + ((d.id_famille_achat != null) ? "<i data-content='" + ((d.id_famille_achat == d.id_famille_depot) ? "Rendu" : "Vendu") + "' class='popup " + ((d.id_famille_achat == d.id_famille_depot) ? "external" : "check circle") + " icon'></i>" : "") + "</td></tr>";
                    total = total + parseFloat(d.prix);
                });
                $('#liste_depots').html(content + "<tr><td colspan='5' class='right aligned'>Total des livres déposés : <strong>" + total.toFixed(2) +" €</strong></td><td></td></tr></tbody></table>");
                $('.popup').popup();
                $('#legende_depot').css('display', '');
            } else {
                $('#liste_depots').html("<div class='not_found'>" + msg.error + "</div>");
            }
        });

        //dossier
        $.ajax({
            type: "GET",
            url: "/api/depots/" + id
        }).done(function (msg) {
            if (msg.status == 200) {
                $('#recapitulatif_depot').css('display', '');
                $('#recapitulatif_depot_montant_vente').html(msg.depot.montant_vendu + " €");
                $('#recapitulatif_depot_frais_dossier').html(msg.depot.frais + " €");
                $('#recapitulatif_depot_frais_envoi').html(msg.depot.frais_envoi + " €");
                //$('#recapitulatif_depot_montant_paye').html(msg.depot.montant_paye + " €");
                var total = msg.depot.montant_vendu - msg.depot.frais - msg.depot.frais_envoi - msg.depot.montant_paye;
                $('#recapitulatif_depot_montant').html(((total > 0) ? total.toFixed(2) : "0") + " €");
                if (msg.depot.appliquer_frais == 1) {
                    $('#checkbox_frais_envoi').html("<div class='ui checkbox' id='appliquer_frais_envoi'><input type='checkbox' checked><label>Appliquer les frais d'envoi</label></div>");
                } else {
                    $('#checkbox_frais_envoi').html("<div class='ui checkbox' id='appliquer_frais_envoi'><input type='checkbox'><label>Appliquer les frais d'envoi</label></div>");
                }
                $('#appliquer_frais_envoi').checkbox({
                    onChange: function () {
                        var appliquer = ($(this).prop('checked') ? 1 : 0);
                        $.ajax({
                            type: "POST",
                            url: "/api/depot/frais_envoi",
                            data: {id_famille: id, frais: appliquer}
                        }).done(function (msg) {
                            if (msg.status == 200) {
                                initDepot();
                            } else {
                                $('#modal_erreur .content').html(msg.error);
                                $('#modal_erreur').modal('show');
                            }
                        });
                    }
                });
            }
        });
    }

    function initAchat () {
        var id = $('#info_famille').data('id');
        //achats
        $.ajax({
            type: "GET",
            url: "/api/achats/familles/" + id
        }).done(function (msg) {
            if (msg.status == 200) {
                var content = "<table class='ui celled striped table'><thead><tr><th>Code exemplaire</th><th>Titre</th><th class='center aligned'>Etat</th><th class='right aligned'>Tarif</th></tr></thead><tbody>";
                msg.achat.forEach(function (d) {
                    content += "<tr><td>" + d.id + "</td><td>" + d.manuel.titre + "</td><td class='center aligned'>" + d.etat.libelle + "</td><td class='right aligned'>" + d.prix + " €</td></tr>";
                });
                $('#liste_achats').html(content + "</tbody></table>");
                $('.popup').popup();
            } else {
                $('#liste_achats').html("<div class='not_found'>" + msg.error + "</div>");
            }
        });

        //dossier
        $.ajax({
            type: "GET",
            url: "/api/achats/" + id
        }).done(function (msg) {
            if (msg.status == 200) {
                $('#recapitulatif_achat').css('display', '');
                $('#recapitulatif_achat_montant_achat').html(msg.achat.montant + " €");
                $('#recapitulatif_achat_frais_dossier').html(msg.achat.frais + " €");
                var total = parseFloat(msg.achat.solde) + parseFloat(msg.achat.frais);
                $('#recapitulatif_achat_montant').html(((total > 0) ? total.toFixed(2) : "0") + " €");
                if (total <= 0) {
                    $('#nouveau_paiement').css('display', 'none');
                }
                $.ajax({
                    type: "GET",
                    url: "/api/mode_paiements"
                }).done(function (msg) {
                    if (msg.status == 200) {
                        var content_mode = "";
                        msg.modes.forEach(function (m) {
                            content_mode += "<option value='" + m.id + "''>" + m.libelle + "</option>";
                        });
                        $('#achat_paiement_mode').html(content_mode);
                        $('#achat_paiement_mode').dropdown();
                    } else {
                        $('#form_achat_paiement').html("<div class='not_found'>Veuillez configurer les modes de paiements.</div>");
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "/api/paiements/famille/" + id
                }).done(function (msg) {
                    if (msg.status == 200) {
                        var total_paiement = 0;
                        var content_paiement = "<table class='ui celled striped table' style='margin-top: 5px'><thead><tr><th>Date</th><th>Mode</th><th>Montant</th></tr></thead><tbody>";
                        msg.paiements.forEach(function (p) {
                            content_paiement += "<tr><td>" + p.date + "</td><td>" + p.mode.libelle + "</td><td>" + p.montant + " €</td></tr>";
                            total_paiement = total_paiement + parseFloat(p.montant);
                        });
                        $('#achat_liste_paiements').html(content_paiement + "</tbody></table>");
                        $('#recapitulatif_achat_montant_paiement').html(total_paiement.toFixed(2) + " €");
                    } else {
                        $('#achat_liste_paiements').html("<div class='not_found'>Aucun paiement pour cette famille.</div>");
                    }
                });
            }
        });
    }
    
    function closeNotif() {
        $('.notification').slideUp('fast');
    }
});